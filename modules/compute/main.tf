resource "google_compute_instance" "compute-apps1" {
  name         = "compute-apps1-${var.env}"
  machine_type = "n1-standard-1"
  project      = "terraform-poc-325614"
  zone         = "europe-west2-a"
  boot_disk {
    initialize_params {
      image = "ubuntu-1604-lts"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
  labels = merge(var.common_labels, {
    name = "compute-instance-${var.env}",
  })
}

resource "google_compute_instance" "compute-apps2" {
  name         = "compute-apps2-${var.env}"
  machine_type = "n1-standard-1"
  project      = "terraform-poc-325614"
  zone         = "europe-west2-a"
  boot_disk {
    initialize_params {
      image = "ubuntu-1604-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
  labels = merge(var.common_labels, {
    name = "compute-instance-${var.env}"
  })
}

