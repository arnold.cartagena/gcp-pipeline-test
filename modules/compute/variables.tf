//Environment
variable "env" {
  type        = string
  description = "Environment"
}

//TAGS
variable "common_labels" {
  type        = map(any)
  description = "GCP Resources Common Tags"
}
