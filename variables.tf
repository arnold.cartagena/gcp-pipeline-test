//Environment
variable "env" {
  type        = string
  description = "Environment"
}

//TAGS
variable "common_tags" {
  type        = map(any)
  description = "GCP Resources Common Tags"
  default     = {}
}

variable "app_version" {
  type        = string
  description = "The version key shall be used to designate the specific version of the application (above)."
}

variable "support_contact" {
  type        = string
  description = "The support_email key shall be used to designate the individual/team associated with the given AWS resource."
}

variable "project_number" {
  type        = string
  description = "This is to track AWS workloads that are not opeartionalized and are currently funded under a SOW or Project"
}

variable "provisioned_by" {
  type        = string
  description = "This tag will help provide insight into how are resources being created on AWS (Terraform, CloudFormation, AWSCLI, AWSConsole)"
}

variable "sourcecode_project_name" {
  type        = string
  description = "The source code project name -> the resource creator"
}
