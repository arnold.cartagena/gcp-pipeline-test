//Global Environment
env = "test"

//COMMON TAGS
support_contact         = "core-team"
project_number          = "dh1"
provisioned_by          = "terraform"
app_version             = "0-0-1"
sourcecode_project_name = "test-project-poc"
