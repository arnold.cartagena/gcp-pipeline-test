module "compute" {
  source = "./modules/compute"
  //Environment
  env = var.env

  common_labels = {
    app_version             = var.app_version
    application             = "compute-${var.env}"
    environment             = var.env
    project_number          = var.project_number
    provisioned_by          = var.provisioned_by
    sourcecode_project_name = var.sourcecode_project_name
    support_contact         = var.support_contact
  }
}
